package org.bitbucket.arvade.globalchatcommands;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bitbucket.arvade.mcregistrator.CommandRegistrar;
import org.bitbucket.arvade.mcregistrator.EventRegistrar;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class GlobalChatPlugin extends JavaPlugin {

    private static final String COMMAND_PACKAGE_PATH = "org.bitbucket.arvade.globalchatcommands.command";
    private static final String EVENT_PACKAGE_PATH = "org.bitbucket.arvade.globalchatcommands.event";
    private Injector injector;

    @Override
    public void onEnable() {
        this.loadConfig();
        GlobalChatPluginModule globalChatPluginModule = new GlobalChatPluginModule(this);
        this.injector = Guice.createInjector(globalChatPluginModule);
        registerCommands();
        registerEvents();
    }

    private void registerCommands() {
        CommandRegistrar commandRegistrar = this.injector.getInstance(CommandRegistrar.class);
        commandRegistrar.registerCommands(COMMAND_PACKAGE_PATH);
    }

    private void registerEvents() {
        EventRegistrar eventRegistrar = this.injector.getInstance(EventRegistrar.class);
        eventRegistrar.registerEvents(EVENT_PACKAGE_PATH);
    }

    private void loadConfig() {
        FileConfiguration fileConfiguration = this.getConfig();
        fileConfiguration.options().copyDefaults(true);
        fileConfiguration.options().copyHeader(true);
        this.saveConfig();
    }
}
