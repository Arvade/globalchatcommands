package org.bitbucket.arvade.globalchatcommands.provider;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.ChatManipulationStrategy;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.CompositeChatManipulationStrategy;
import org.bitbucket.arvade.mcregistrator.PackageScanner;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CompositeChatManipulationStrategyProvider implements Provider<CompositeChatManipulationStrategy> {

    private static final String DEFAULT_CHAT_MANIPULATION_STRATEGIES_PACKAGE = "org.bitbucket.arvade.globalchatcommands.command.manipulation.impl";
    private PackageScanner packageScanner;
    private Injector injector;

    @Inject
    public CompositeChatManipulationStrategyProvider(PackageScanner packageScanner, Injector injector) {
        this.packageScanner = packageScanner;
        this.injector = injector;
    }

    @Override
    public CompositeChatManipulationStrategy get() {
        Collection<Class<?>> classes = packageScanner.findClasses(DEFAULT_CHAT_MANIPULATION_STRATEGIES_PACKAGE);

        List<ChatManipulationStrategy> strategyList = classes.stream()
                .filter(ChatManipulationStrategy.class::isAssignableFrom)
                .map(aClass -> injector.getInstance(aClass))
                .map(o -> (ChatManipulationStrategy) o)
                .collect(Collectors.toList());

        CompositeChatManipulationStrategy.Builder builder = CompositeChatManipulationStrategy.builder();
        for (ChatManipulationStrategy strategy : strategyList) {
            builder.register(strategy);
        }
        return builder.build();
    }
}
