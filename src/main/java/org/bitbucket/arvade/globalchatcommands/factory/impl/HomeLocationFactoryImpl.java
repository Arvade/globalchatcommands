package org.bitbucket.arvade.globalchatcommands.factory.impl;

import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.factory.HomeLocationFactory;
import org.bitbucket.arvade.globalchatcommands.service.HomeService;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import javax.inject.Named;

public class HomeLocationFactoryImpl implements HomeLocationFactory {

    private HomeService homeService;

    @Inject
    public HomeLocationFactoryImpl(
            @Named("HomeServiceTransactional") HomeService homeService) {
        this.homeService = homeService;
    }

    @Override
    public void createHomeLocationForPlayer(Player player, Location location) {
        HomeLocationEntity homeLocation = new HomeLocationEntity(player.getUniqueId(), location);
        this.homeService.save(homeLocation);
    }
}
