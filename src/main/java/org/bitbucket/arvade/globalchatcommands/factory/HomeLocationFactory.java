package org.bitbucket.arvade.globalchatcommands.factory;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface HomeLocationFactory {
    void createHomeLocationForPlayer(Player player, Location location);
}
