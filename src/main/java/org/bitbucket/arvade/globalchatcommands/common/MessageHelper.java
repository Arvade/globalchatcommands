package org.bitbucket.arvade.globalchatcommands.common;

import org.bukkit.entity.Player;

public interface MessageHelper {

    String translateColorCodes(String textToTranslate);

    void translateAndSendMessage(Player target, String textToTranslate);

    void splitTranslateAndSendMessages(Player target, String message);
}
