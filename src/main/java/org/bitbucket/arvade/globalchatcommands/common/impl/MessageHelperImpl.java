package org.bitbucket.arvade.globalchatcommands.common.impl;

import lombok.Getter;
import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class MessageHelperImpl implements MessageHelper {

    private static final char ALT_COLOR_CHAR = '&';

    @Setter
    @Getter
    private char altColorChar = ALT_COLOR_CHAR;

    @Override
    public void translateAndSendMessage(Player target, String textToTranslate) {
        String translatedMessage = translateColorCodes(textToTranslate);
        target.sendMessage(translatedMessage);
    }

    @Override
    public String translateColorCodes(String textToTranslate) {
        return ChatColor.translateAlternateColorCodes(ALT_COLOR_CHAR, textToTranslate);
    }

    @Override
    public void splitTranslateAndSendMessages(Player target, String message) {
        String[] splitedMessages = message.split("\n");
        Arrays.stream(splitedMessages)
                .forEach(s -> translateAndSendMessage(target, s));
    }
}
