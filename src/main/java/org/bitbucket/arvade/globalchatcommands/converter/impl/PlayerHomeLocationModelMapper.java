package org.bitbucket.arvade.globalchatcommands.converter.impl;

import org.bitbucket.arvade.globalchatcommands.converter.GenericModelMapper;
import org.bitbucket.arvade.globalchatcommands.converter.LocationConverter;
import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.model.LocationEntity;
import org.bitbucket.arvade.globalchatcommands.model.PlayerHomeLocation;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;

import javax.inject.Inject;
import java.util.Date;

public class PlayerHomeLocationModelMapper implements GenericModelMapper<PlayerHomeLocation, HomeLocationEntity> {

    private Server server;

    @Inject
    public PlayerHomeLocationModelMapper(Server server) {
        this.server = server;
    }

    @Override
    public HomeLocationEntity convertToDTO(PlayerHomeLocation playerHomeLocation) {
        HomeLocationEntity dataTransferObject = new HomeLocationEntity();
        LocationEntity locationEntity = playerHomeLocation.getLocation();
        World world = this.server.getWorld(locationEntity.getWorldUUID());
        Location location = new Location(world,
                locationEntity.getX(),
                locationEntity.getY(),
                locationEntity.getZ());

        dataTransferObject.setLocation(location);
        dataTransferObject.setPlayerUUID(playerHomeLocation.getPlayerUUID());

        return dataTransferObject;
    }

    @Override
    public PlayerHomeLocation convertToEntityAttribute(HomeLocationEntity homeLocationEntity) {
        PlayerHomeLocation playerHomeLocation = new PlayerHomeLocation();

        LocationConverter locationConverter = new LocationConverter(this.server);
        LocationEntity locationEntity = locationConverter.convertToDatabaseColumn(homeLocationEntity.getLocation());
        //TODO: bind this converter and inject
        playerHomeLocation.setLocation(locationEntity);
        playerHomeLocation.setPlayerUUID(homeLocationEntity.getPlayerUUID());
        playerHomeLocation.setNickname(server.getPlayer(homeLocationEntity.getPlayerUUID()).getDisplayName());
        playerHomeLocation.setSetHomeDate(new Date());

        return playerHomeLocation;
    }
}
