package org.bitbucket.arvade.globalchatcommands.converter;

import org.bitbucket.arvade.globalchatcommands.model.LocationEntity;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;

import javax.inject.Inject;
import javax.persistence.AttributeConverter;
import java.util.UUID;

public class LocationConverter implements AttributeConverter<Location, LocationEntity> {

    private Server server;

    @Inject
    public LocationConverter(Server server) {
        this.server = server;
    }

    @Override
    public LocationEntity convertToDatabaseColumn(Location location) {
        UUID worldUUID = location.getWorld().getUID();
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();

        return new LocationEntity(worldUUID, x, y, z);
    }

    @Override
    public Location convertToEntityAttribute(LocationEntity dbData) {
        World world = this.server.getWorld(dbData.getWorldUUID());
        int x = dbData.getX();
        int y = dbData.getY();
        int z = dbData.getZ();

        return new Location(world, x, y, z);
    }
}
