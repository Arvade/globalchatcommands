package org.bitbucket.arvade.globalchatcommands.converter;

public interface GenericModelMapper<X, Y> {
    Y convertToDTO(X x);

    X convertToEntityAttribute(Y y);
}
