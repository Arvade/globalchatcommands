package org.bitbucket.arvade.globalchatcommands.model;

import lombok.Getter;
import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.converter.UUIDConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
public class PlayerMute {

    @Id
    private Long id;

    @Column(nullable = false)
    @Convert(converter = UUIDConverter.class)
    private UUID playerUUID;

    @Column
    private long duration;

    @Column
    private String reason;
}
