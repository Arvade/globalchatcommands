package org.bitbucket.arvade.globalchatcommands.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.converter.UUIDConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Table
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PlayerHomeLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nickname;

    @Column(nullable = false)
    @Convert(converter = UUIDConverter.class)
    private UUID playerUUID;

    @Column(nullable = false)
    @Embedded
    private LocationEntity location;

    @Column(nullable = false)
    private Date setHomeDate = new Date();
}