package org.bitbucket.arvade.globalchatcommands.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.converter.UUIDConverter;

import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.util.UUID;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LocationEntity {

    @Convert(converter = UUIDConverter.class)
    private UUID worldUUID;

    private int x;

    private int y;

    private int z;
}
