package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.HELP_MSG_PATH;
import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.NO_PLAYER_MESSAGE_PATH;

public class HelpCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "pomoc";

    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;

    @Inject
    public HelpCommand(FileConfiguration pluginConfig, MessageHelper messageHelper) {
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 0) {
            return false;
        }

        Player player = (Player) commandSender;

        String message = pluginConfig.getString(HELP_MSG_PATH);
        messageHelper.splitTranslateAndSendMessages(player, message);

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
