package org.bitbucket.arvade.globalchatcommands.command.manipulation;

import org.bukkit.entity.Player;

public interface ChatManipulationStrategy {
    void accept(Player player, String strategyName, String[] args);

    boolean hasPermission(Player player);

    boolean supports(String strategyName);
}
