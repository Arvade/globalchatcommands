package org.bitbucket.arvade.globalchatcommands.command.manipulation;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CompositeChatManipulationStrategy implements ChatManipulationStrategy {

    private List<ChatManipulationStrategy> strategies;

    public CompositeChatManipulationStrategy(List<ChatManipulationStrategy> strategies) {
        this.strategies = strategies;
    }

    @Override
    public void accept(Player player, String strategyName, String[] args) {
        strategies.stream()
                .filter(chatManipulationStrategy -> chatManipulationStrategy.supports(strategyName))
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Strategy for " + strategyName + " not found"))
                .accept(player, strategyName, args);
    }

    @Override
    public boolean hasPermission(Player player) {
        return false;
    }

    @Override
    public boolean supports(String strategyName) {
        return strategies
                .stream()
                .anyMatch(chatManipulationStrategy -> chatManipulationStrategy.supports(strategyName));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private List<ChatManipulationStrategy> chatManipulationStrategies = new ArrayList<>();

        public Builder register(ChatManipulationStrategy chatManipulationStrategy) {
            chatManipulationStrategies.add(chatManipulationStrategy);
            return this;
        }

        public CompositeChatManipulationStrategy build() {
            return new CompositeChatManipulationStrategy(chatManipulationStrategies);
        }
    }
}
