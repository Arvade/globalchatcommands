package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.service.MessageService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.NO_PERMISSIONS_MESSAGE_PATH;
import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.NO_PLAYER_MESSAGE_PATH;

public class BlockReceivedPrivateMessagesCommand implements NamedCommandExecutor {

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.wygwizdane";

    private static final String DEFAULT_NAME = "wygwizdane";


    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageService messageService;
    private MessageHelper messageHelper;

    @Inject
    public BlockReceivedPrivateMessagesCommand(FileConfiguration pluginConfig, MessageService messageService, MessageHelper messageHelper) {
        this.pluginConfig = pluginConfig;
        this.messageService = messageService;
        this.messageHelper = messageHelper;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }

        if (args.length != 0) {
            return false;
        }

        Player player = (Player) commandSender;

        if (player.hasPermission(REQUIRED_PERMISSIONS)) {
            this.messageService.toggleBlock(player);
        } else {
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
