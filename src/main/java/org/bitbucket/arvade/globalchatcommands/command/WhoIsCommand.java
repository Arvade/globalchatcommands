package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.GameMode;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.net.InetAddress;
import java.util.Optional;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class WhoIsCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "whois";

    private static final String REQUIRED_PERMISSION = "globalchatcommand.whois";
    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;
    private Server server;

    @Inject
    public WhoIsCommand(FileConfiguration pluginConfig, MessageHelper messageHelper, Server server) {
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
        this.server = server;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 1) {
            return false;
        }
        Player player = (Player) commandSender;
        String providedTargetName = args[0];

        printInfoIfPlayerHasPermission(player, providedTargetName);
        return true;
    }

    private void printInfoIfPlayerHasPermission(Player commandSender, String providedTargetNickname) {
        if (commandSender.hasPermission(REQUIRED_PERMISSION)) {
            printInfoIfTargetExists(commandSender, providedTargetNickname);
        } else {
            messageHelper.translateAndSendMessage(commandSender, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }
    }

    private void printInfoIfTargetExists(Player commandSender, String providedTargetNickname) {
        Optional<Player> targetOptional = Optional.ofNullable(this.server.getPlayer(providedTargetNickname));
        if (targetOptional.isPresent()) {
            Player target = targetOptional.get();
            printInfoAboutTarget(commandSender, target);
        } else {
            String formattedMessage = String.format(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH), providedTargetNickname);
            messageHelper.translateAndSendMessage(commandSender, formattedMessage);
        }
    }

    private void printInfoAboutTarget(Player commandSender, Player target) {
        CraftPlayer craftPlayer = (CraftPlayer) target;
        /*  &8» Gildia &2{TAG}\n
                &8» Zdrowie: &cxx/20\n
                &8» Glod: &cxx/20\n
                &8» Ping: &c{X} (np &c100)\n
                &8» Fly: &c{X} (Brak - &cBrak / Posiadanie - &cAktywne)\n
                &8» God: &c{X} (Brak - &cBrak / Posiadanie - &cAktywne)\n
                &8» Gamemode: &c{X} (Brak - &cBrak / Posiadanie - &cAktywne)\n
                &8» Wyciszenie: &c{X} (Brak - &cBrak / Posiadanie - &cAktywne do: xx.xx.xxxx xx.xx)\n
                &8» Adres IP: &c{X} (np: &c100.100.100.100)\n"*/
        InetAddress address = target.getAddress().getAddress();
        GameMode gameMode = target.getGameMode();

        String targetGuild = "gildia"; //TODO: getGuild from database
        double targetHealth = target.getHealth();
        int targetFoodLevel = target.getFoodLevel();
        int targetPing = craftPlayer.getHandle().ping;
        boolean isTargetFlying = target.isFlying();
        boolean isGod = false; //TODO: isGod?
        String isMuted = "Brak"; //TODO: czy wyciszony?
        String targetGameMode = gameMode.toString();

        String formattedAddress = address.toString().replaceAll("/", "");


        //TODO: Zrobic WhoIsEntity z builderem
        String formattedInfo = String
                .format(pluginConfig.getString(WHO_IS),
                        target.getDisplayName(),
                        targetGuild,
                        targetHealth,
                        targetFoodLevel,
                        targetPing,
                        isTargetFlying,
                        isMuted,
                        isGod,
                        targetGameMode,
                        formattedAddress);
        messageHelper.splitTranslateAndSendMessages(commandSender, formattedInfo);
    }

    @Override
    public String getName() {
        return commandName;
    }
}




























































