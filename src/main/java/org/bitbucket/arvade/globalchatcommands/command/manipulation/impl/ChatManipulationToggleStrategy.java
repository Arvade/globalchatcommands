package org.bitbucket.arvade.globalchatcommands.command.manipulation.impl;

import com.google.inject.Inject;
import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.ChatManipulationStrategy;
import org.bitbucket.arvade.globalchatcommands.service.MessageService;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatManipulationToggleStrategy implements ChatManipulationStrategy {

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.disable";
    private static final String STRATEGY_NAME = "toogle";

    @Setter
    private String strategyName = STRATEGY_NAME;

    private MessageService messageService;

    @Inject
    public ChatManipulationToggleStrategy(MessageService messageService) {
        this.messageService = messageService;
    }

    @Override
    public void accept(Player player, String strategyName, String[] args) {
        if (!hasPermission(player)) {
            throw new IllegalStateException("Player " + player.getDisplayName() + " doesn't have permissions");
        }

        if (!supports(strategyName)) {
            return;
        }
        boolean isChatEnabled = messageService.isChatEnabled();
        messageService.setChat(!isChatEnabled);
        player.sendMessage(ChatColor.GREEN + "Chat has been " + (messageService.isChatEnabled() ? "enabled" : "disabled"));
    }

    @Override
    public boolean hasPermission(Player player) {
        return player.hasPermission(REQUIRED_PERMISSIONS);
    }

    @Override
    public boolean supports(String strategyName) {
        return strategyName.equalsIgnoreCase(this.strategyName);
    }
}
