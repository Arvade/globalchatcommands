package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.inject.Inject;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.NO_PERMISSIONS_MESSAGE_PATH;
import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.NO_PLAYER_MESSAGE_PATH;

public class FoodCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "jedzenie";

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.jedzenie";

    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;

    @Inject
    public FoodCommand(FileConfiguration pluginConfig, MessageHelper messageHelper) {
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String message = pluginConfig.getString(NO_PLAYER_MESSAGE_PATH);
            String translatedMessage = messageHelper.translateColorCodes(message);
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 0) {
            return false;
        }
        Player player = (Player) commandSender;
        addFoodIfPlayerHasPermissions(player, Material.COOKED_BEEF);
        return true;
    }

    private void addFoodIfPlayerHasPermissions(Player player, Material material) {
        if (player.hasPermission(REQUIRED_PERMISSIONS)) {
            Inventory playerInventory = player.getInventory();
            ItemStack itemStack = new ItemStack(material);
            playerInventory.addItem(itemStack);
            playerInventory.addItem(itemStack);
        } else {
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }
    }

    @Override
    public String getName() {
        return commandName;
    }
}
