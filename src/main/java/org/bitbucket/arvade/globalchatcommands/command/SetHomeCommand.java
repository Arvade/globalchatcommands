package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.factory.HomeLocationFactory;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class SetHomeCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "sethome";

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.sethome";

    @Setter
    private String commandName = DEFAULT_NAME;

    private HomeLocationFactory homeLocationFactory;
    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;

    @Inject
    public SetHomeCommand(HomeLocationFactory homeLocationFactory, FileConfiguration pluginConfig, MessageHelper messageHelper) {
        this.homeLocationFactory = homeLocationFactory;
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 0) {
            return false;
        }
        Player player = (Player) commandSender;
        if (player.hasPermission(REQUIRED_PERMISSIONS)) {
            Location currentPlayerLocation = player.getLocation();
            this.homeLocationFactory.createHomeLocationForPlayer(player, currentPlayerLocation);
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(SET_HOME_SUCCESSFUL));
        } else {
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
