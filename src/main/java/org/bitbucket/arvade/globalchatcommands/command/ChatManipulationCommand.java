package org.bitbucket.arvade.globalchatcommands.command;

import com.google.inject.Inject;
import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.ChatManipulationStrategy;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatManipulationCommand implements NamedCommandExecutor {

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.chatmanipulation";
    private static final String DEFAULT_NAME = "chat";

    @Setter
    private String commandName = DEFAULT_NAME;

    private ChatManipulationStrategy chatManipulationStrategy;

    @Inject
    public ChatManipulationCommand(ChatManipulationStrategy chatManipulationStrategy) {
        this.chatManipulationStrategy = chatManipulationStrategy;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must be player to use this command");
        }
        Player player = (Player) commandSender;
        processIfPlayerHasPermission(player, args);
        return true;
    }

    private void processIfPlayerHasPermission(Player player, String[] args) {
        if (player.hasPermission(REQUIRED_PERMISSIONS)) {
            String strategyName = args[0];
            try {
                chatManipulationStrategy.accept(player, strategyName, args);
            } catch (IllegalStateException e) {
                Logger.getAnonymousLogger().log(Level.WARNING, "No strategy found for " + strategyName);
                player.sendMessage(ChatColor.RED + "Brak opcji " + ChatColor.BLUE + strategyName + ChatColor.RED + " dla komendy " + ChatColor.BLUE + this.commandName);
            }
        }
    }

    @Override
    public String getName() {
        return commandName;
    }

}
