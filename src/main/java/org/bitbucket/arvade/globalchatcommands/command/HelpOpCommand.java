package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class HelpOpCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "helpop";

    private static final String REQUIRED_PERMISSION = "globalchatcommands.helpop";
    private static final String OP_PERMISSION = "globalchatcommands.op";

    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;
    private Server server;

    @Inject
    public HelpOpCommand(FileConfiguration pluginConfig, MessageHelper messageHelper, Server server) {
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
        this.server = server;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length < 2) {
            return false;
        }

        Player player = (Player) commandSender;
        StringBuilder providedMessage = new StringBuilder();

        for (String string : args) {
            providedMessage.append(" ").append(string);
        }

        askForHelpIfPlayerHasPermission(player, providedMessage.toString());

        return true;
    }

    private void askForHelpIfPlayerHasPermission(Player player, String message) {
        if (player.hasPermission(REQUIRED_PERMISSION)) {
            askForHelp(player, message);
        } else {
            messageHelper.translateAndSendMessage(player, NO_PERMISSIONS_MESSAGE_PATH);
        }
    }

    private void askForHelp(Player player, String message) {
        List<? extends Player> onlineOps = getOnlineOps();
        if (onlineOps.isEmpty()) {
            player.sendMessage(ChatColor.RED + "There's no online op. Please try later");
        } else {
            String formattedMessage = String.format(pluginConfig.getString(HELP_OP), message);
            onlineOps.forEach(o -> messageHelper.splitTranslateAndSendMessages(o, formattedMessage));
        }
    }

    private List<? extends Player> getOnlineOps() {
        return this.server.getOnlinePlayers()
                .stream()
                .filter(player -> player.hasPermission(OP_PERMISSION))
                .collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return commandName;
    }
}
