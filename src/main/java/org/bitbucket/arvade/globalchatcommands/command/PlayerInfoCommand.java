package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.Optional;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class PlayerInfoCommand implements NamedCommandExecutor {


    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.gracz";

    private static final String DEFAULT_NAME = "gracz";

    @Setter
    private String commandName = DEFAULT_NAME;

    private MessageHelper messageHelper;
    private FileConfiguration pluginConfig;
    private Server server;


    @Inject
    public PlayerInfoCommand(MessageHelper messageHelper, FileConfiguration pluginConfig, Server server) {
        this.messageHelper = messageHelper;
        this.pluginConfig = pluginConfig;
        this.server = server;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }

        if (args.length != 1) {
            return false;
        }
        Player player = (Player) commandSender;
        String providedTargetNickname = args[0];

        printTargetInfoIfCommandSenderHasPermissions(player, providedTargetNickname);
        return true;
    }

    private void printTargetInfoIfCommandSenderHasPermissions(Player commandSender, String providedTargetName) {
        if (commandSender.hasPermission(REQUIRED_PERMISSIONS)) {
            printTargetInfoIfTargetExists(commandSender, providedTargetName);
        } else {
            messageHelper.translateAndSendMessage(commandSender, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }
    }

    private void printTargetInfoIfTargetExists(Player commandSender, String providedTargetName) {
        Optional<Player> targetOptional = Optional.ofNullable(this.server.getPlayer(providedTargetName));
        if (targetOptional.isPresent()) {
            Player target = targetOptional.get();
            String targetNickname = target.getDisplayName();
            //TODO: Get Target's guild
            //TODO: Get Target's points
            //TODO: Get Target's kills
            //TODO: Get Target's deaths
            //TODO: Get Target's KDA
            //TODO: Get Target's isOnline()
            //TODO: Get full player detailed info. as a mapped entity from database
            boolean isTargetOnline = target.isOnline();

            String formattedMessage = String
                    .format(pluginConfig.getString(PLAYER_INFO_MESSAGE_PATH),
                            targetNickname,
                            "gildia",
                            "punkty",
                            "zabojstwa",
                            "smierci",
                            "KDA",
                            isTargetOnline);

            messageHelper.splitTranslateAndSendMessages(commandSender, formattedMessage);
        } else {
            String formattedMessage = String.format(pluginConfig.getString(NO_TARGET_MESSAGE_PATH), providedTargetName);
            messageHelper.translateAndSendMessage(commandSender, formattedMessage);
        }
    }

    @Override
    public String getName() {
        return commandName;
    }
}
