package org.bitbucket.arvade.globalchatcommands.command.argument;

public class CommandMessagesPath {

    public static final String NO_PERMISSIONS_MESSAGE_PATH = "messages.permissions_message";
    public static final String NO_TARGET_MESSAGE_PATH = "messages.no_target_message";
    public static final String NO_PLAYER_MESSAGE_PATH = "messages.no_player_message";
    public static final String PLAYER_WANT_TO_TP_PATH = "messages.player_want_to_tp";
    public static final String NO_REQUEST_SENT_PATH = "messages.no_request_message";
    public static final String SET_HOME_SUCCESSFUL = "messages.set_home_successful";
    public static final String HOME_NOT_SET_MESSAGE_PATH = "messages.home_not_set";
    public static final String PLAYER_INFO_MESSAGE_PATH = "messages.player_info";
    public static final String TP_INFO_PATH = "messages.tp_info_message";
    public static final String HELP_MSG_PATH = "messages.pomoc";
    public static final String REQUEST_ALREADY_SEND_MESSAGE_PATH = "messages.request_already_sent";
    public static final String HELP_OP = "messages.helpop";
    public static final String WHO_IS = "messages.whois";
    public static final String UNBLOCK_MSG = "messages.unblock_msg_successful";
    public static final String BLOCK_MSG = "messages.block_msg_successful";
    public static final String TARGET_HAS_BLOCKED_MSG = "messages.target_has_blocked_msg";
    public static final String PRIVATE_MESSAGE_BOILERPLATE = "messages.private_message";

    private CommandMessagesPath() {
    }

}
