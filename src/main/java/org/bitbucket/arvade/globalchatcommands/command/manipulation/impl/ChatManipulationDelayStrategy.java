package org.bitbucket.arvade.globalchatcommands.command.manipulation.impl;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.ChatManipulationStrategy;
import org.bukkit.entity.Player;

public class ChatManipulationDelayStrategy implements ChatManipulationStrategy {

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.manipulation";
    private static final String STRATEGY_NAME = "delay";

    @Setter
    private String strategyName = STRATEGY_NAME;

    @Override
    public void accept(Player player, String strategyName, String[] args) {
        if (!hasPermission(player)) {
            throw new IllegalStateException("Player " + player.getDisplayName() + " doesn't have permissions");
        }

        if (!supports(strategyName)) {
            return;
        }
        //TODO: Set manipulation for each player's message
    }

    @Override
    public boolean hasPermission(Player player) {
        return player.hasPermission(REQUIRED_PERMISSIONS);
    }

    @Override
    public boolean supports(String strategyName) {
        return strategyName.equalsIgnoreCase(this.strategyName);
    }
}
