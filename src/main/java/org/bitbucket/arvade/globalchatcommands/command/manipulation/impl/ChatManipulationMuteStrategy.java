package org.bitbucket.arvade.globalchatcommands.command.manipulation.impl;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.ChatManipulationStrategy;
import org.bukkit.entity.Player;

public class ChatManipulationMuteStrategy implements ChatManipulationStrategy {

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.mute";
    private static final String STRATEGY_NAME = "mute";

    //TODO: Mute service

    @Setter
    private String strategyName = STRATEGY_NAME;

    @Override
    public void accept(Player player, String strategyName, String[] args) {
        if (!hasPermission(player)) {
            throw new IllegalStateException("Player " + player.getDisplayName() + " doesn't have permissions");
        }

        if (!supports(strategyName)) {
            return;
        }

        //TODO: Mute
    }

    @Override
    public boolean hasPermission(Player player) {
        return player.hasPermission(REQUIRED_PERMISSIONS);
    }

    @Override
    public boolean supports(String strategyName) {
        return strategyName.equalsIgnoreCase(this.strategyName);
    }
}
