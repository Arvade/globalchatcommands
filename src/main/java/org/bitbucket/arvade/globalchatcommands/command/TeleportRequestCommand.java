package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.service.TeleportService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.Optional;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class TeleportRequestCommand implements NamedCommandExecutor {


    private static final String DEFAULT_NAME = "tpa";

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.tpa";


    @Setter
    private String commandName = DEFAULT_NAME;

    private TeleportService teleportService;
    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;
    private Server server;

    @Inject
    public TeleportRequestCommand(TeleportService teleportService, FileConfiguration pluginConfig, MessageHelper messageHelper, Server server) {
        this.teleportService = teleportService;
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
        this.server = server;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 1) {
            return false;
        }

        String providedTargetName = args[0];
        Player requestSender = (Player) commandSender;

        teleportIfHasPermissions(requestSender, providedTargetName);

        return true;
    }

    private void teleportIfHasPermissions(Player requestSender, String providedTargetName) {
        if (requestSender.hasPermission(REQUIRED_PERMISSIONS)) {
            teleportIfTargetExists(requestSender, providedTargetName);
        } else {
            messageHelper.translateAndSendMessage(requestSender, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }
    }

    private void teleportIfTargetExists(Player requestSender, String providedTargetName) {
        Optional<Player> targetOptional = Optional.ofNullable(server.getPlayer(providedTargetName));
        if (targetOptional.isPresent()) {
            Player target = targetOptional.get();
            if (teleportService.addRequest(requestSender, target)) {
                String message = String.format(pluginConfig.getString(PLAYER_WANT_TO_TP_PATH), requestSender.getDisplayName());
                messageHelper.translateAndSendMessage(target, message);
            } else {
                String message = pluginConfig.getString(REQUEST_ALREADY_SEND_MESSAGE_PATH);
                messageHelper.translateAndSendMessage(target, message);
            }

        } else {
            String message = String.format(pluginConfig.getString(NO_TARGET_MESSAGE_PATH), providedTargetName);
            messageHelper.translateAndSendMessage(requestSender, message);
        }
    }

    @Override
    public String getName() {
        return commandName;
    }
}
