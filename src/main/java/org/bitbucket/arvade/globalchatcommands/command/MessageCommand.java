package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.service.MessageService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.Optional;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class MessageCommand implements NamedCommandExecutor {


    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.msg";

    private static final String DEFAULT_NAME = "msg";

    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageService messageService;
    private MessageHelper messageHelper;
    private Server server;

    @Inject
    public MessageCommand(Server server, FileConfiguration pluginConfig, MessageService messageService, MessageHelper messageHelper) {
        this.server = server;
        this.pluginConfig = pluginConfig;
        this.messageService = messageService;
        this.messageHelper = messageHelper;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length < 2) {
            return false;
        }

        Player player = (Player) commandSender;
        String providedTargetName = args[0];
        StringBuilder messageBuilder = new StringBuilder();

        for (int i = 1; i < args.length; i++) {
            messageBuilder.append(" ").append(args[i]);
        }

        sendMessageIfHasPermissions(player, providedTargetName, messageBuilder.toString());

        return true;
    }

    private void sendMessageIfHasPermissions(Player player, String providedTargetName, String messageForTarget) {
        if (player.hasPermission(REQUIRED_PERMISSIONS)) {
            sendMessageIfTargetExists(player, providedTargetName, messageForTarget);
        } else {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
            player.sendMessage(translatedMessage);
        }
    }

    private void sendMessageIfTargetExists(Player sender, String providedTargetName, String messageForTarget) {
        Optional<Player> targetOptional = Optional.ofNullable(this.server.getPlayer(providedTargetName));
        if (targetOptional.isPresent()) {
            Player target = targetOptional.get();
            this.messageService.sendMessage(sender, target, messageForTarget);
        } else {
            String message = String.format(pluginConfig.getString(NO_TARGET_MESSAGE_PATH), providedTargetName);
            messageHelper.translateAndSendMessage(sender, message);
        }
    }

    @Override
    public String getName() {
        return commandName;
    }
}
