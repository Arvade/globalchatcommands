package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.service.TeleportService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.Optional;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class TeleportResponseCommand implements NamedCommandExecutor {

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.tpaccept";

    private static final String DEFAULT_NAME = "tpaccept";

    @Setter
    private String commandName = DEFAULT_NAME;

    private TeleportService teleportService;
    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;
    private Server server;

    @Inject
    public TeleportResponseCommand(TeleportService teleportService, FileConfiguration pluginConfig, MessageHelper messageHelper, Server server) {
        this.teleportService = teleportService;
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
        this.server = server;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 1) {
            return false;
        }

        String providedRequestSenderName = args[0];
        Player target = (Player) commandSender;

        sendResponseIfPlayerHasPermissions(target, providedRequestSenderName);
        return true;
    }

    private void sendResponseIfPlayerHasPermissions(Player target, String providedRequestSenderName) {
        if (target.hasPermission(REQUIRED_PERMISSIONS)) {
            sendResponseIfRequestSenderExists(target, providedRequestSenderName);
        } else {
            messageHelper.translateAndSendMessage(target, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }
    }

    private void sendResponseIfRequestSenderExists(Player target, String providedRequestSenderName) {
        Optional<Player> requestSenderOptional = Optional.ofNullable(this.server.getPlayer(providedRequestSenderName));
        if (requestSenderOptional.isPresent()) {
            Player requestSender = requestSenderOptional.get();

            if (teleportService.response(requestSender, target)) {
                target.sendMessage(ChatColor.GREEN + "Teleportuję...");

                String formattedInfoMessage = String.format(pluginConfig.getString(TP_INFO_PATH), target.getDisplayName());
                messageHelper.translateAndSendMessage(requestSender, formattedInfoMessage);
            } else {
                String formattedMessage = String.format(pluginConfig.getString(NO_REQUEST_SENT_PATH), requestSender.getDisplayName());
                messageHelper.translateAndSendMessage(target, formattedMessage);
            }
        } else {
            String formattedMessage = String.format(pluginConfig.getString(NO_TARGET_MESSAGE_PATH), providedRequestSenderName);
            messageHelper.translateAndSendMessage(target, formattedMessage);
        }
    }

    @Override
    public String getName() {
        return commandName;
    }
}
