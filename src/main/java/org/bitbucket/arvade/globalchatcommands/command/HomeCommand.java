package org.bitbucket.arvade.globalchatcommands.command;

import lombok.Setter;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.service.AsyncHomeService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

public class HomeCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "home";

    private static final String REQUIRED_PERMISSIONS = "globalchatcommands.home";

    @Setter
    private String commandName = DEFAULT_NAME;

    private FileConfiguration pluginConfig;
    private MessageHelper messageHelper;
    private AsyncHomeService asyncHomeService;

    @Inject
    public HomeCommand(FileConfiguration pluginConfig, MessageHelper messageHelper, AsyncHomeService asyncHomeService) {
        this.pluginConfig = pluginConfig;
        this.messageHelper = messageHelper;
        this.asyncHomeService = asyncHomeService;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            String translatedMessage = messageHelper.translateColorCodes(pluginConfig.getString(NO_PLAYER_MESSAGE_PATH));
            commandSender.sendMessage(translatedMessage);
            return true;
        }
        if (args.length != 0) {
            return false;
        }
        Player player = (Player) commandSender;
        teleportIfPlayerHasPermission(player);

        return true;
    }

    private void teleportIfPlayerHasPermission(Player player) {
        if (player.hasPermission(REQUIRED_PERMISSIONS)) {
            teleportIfHomeExists(player);
        } else {
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(NO_PERMISSIONS_MESSAGE_PATH));
        }
    }

    private void teleportIfHomeExists(Player player) throws RuntimeException {
        asyncHomeService
                .asyncFindByPlayerUUID(player.getUniqueId())
                .thenAccept(homeLocationEntityOptional -> {
                    if (homeLocationEntityOptional.isPresent()) {
                        HomeLocationEntity homeLocationEntity = homeLocationEntityOptional.get();

                        Location homeLocation = homeLocationEntity.getLocation();
                        player.teleport(homeLocation);
                        player.sendMessage(ChatColor.GREEN + "Teleportuje...");
                    } else {
                        messageHelper.translateAndSendMessage(player, pluginConfig.getString(HOME_NOT_SET_MESSAGE_PATH));
                    }
                });
    }

    @Override
    public String getName() {
        return commandName;
    }
}
