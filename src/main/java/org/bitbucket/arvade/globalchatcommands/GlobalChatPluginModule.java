package org.bitbucket.arvade.globalchatcommands;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import org.bitbucket.arvade.globalchatcommands.command.manipulation.ChatManipulationStrategy;
import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.common.impl.MessageHelperImpl;
import org.bitbucket.arvade.globalchatcommands.converter.GenericModelMapper;
import org.bitbucket.arvade.globalchatcommands.converter.impl.PlayerHomeLocationModelMapper;
import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.factory.HomeLocationFactory;
import org.bitbucket.arvade.globalchatcommands.factory.impl.HomeLocationFactoryImpl;
import org.bitbucket.arvade.globalchatcommands.model.PlayerHomeLocation;
import org.bitbucket.arvade.globalchatcommands.provider.CompositeChatManipulationStrategyProvider;
import org.bitbucket.arvade.globalchatcommands.provider.SessionFactoryProvider;
import org.bitbucket.arvade.globalchatcommands.repository.HomeRepository;
import org.bitbucket.arvade.globalchatcommands.repository.impl.HomeRepositoryImpl;
import org.bitbucket.arvade.globalchatcommands.service.AsyncHomeService;
import org.bitbucket.arvade.globalchatcommands.service.HomeService;
import org.bitbucket.arvade.globalchatcommands.service.MessageService;
import org.bitbucket.arvade.globalchatcommands.service.TeleportService;
import org.bitbucket.arvade.globalchatcommands.service.impl.*;
import org.bitbucket.arvade.mcregistrator.CommandRegistrar;
import org.bitbucket.arvade.mcregistrator.EventRegistrar;
import org.bitbucket.arvade.mcregistrator.PackageScanner;
import org.bitbucket.arvade.mcregistrator.impl.CommandRegistrarImpl;
import org.bitbucket.arvade.mcregistrator.impl.EventRegistrarImpl;
import org.bitbucket.arvade.mcregistrator.impl.PackageScannerImpl;
import org.bukkit.Server;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.hibernate.SessionFactory;

public class GlobalChatPluginModule extends AbstractModule {

    private JavaPlugin javaPlugin;

    public GlobalChatPluginModule(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
    }

    @Override
    protected void configure() {
        bind(JavaPlugin.class).toInstance(javaPlugin);
        bind(Server.class).toInstance(javaPlugin.getServer());
        bind(FileConfiguration.class).toInstance(javaPlugin.getConfig());

        bind(HomeService.class).to(HomeServiceImpl.class);
        bind(MessageService.class).to(MessageServiceImpl.class);
        bind(TeleportService.class).to(TeleportServiceImpl.class);

        bind(HomeRepository.class).to(HomeRepositoryImpl.class);
        bind(SessionFactory.class).toProvider(SessionFactoryProvider.class).asEagerSingleton();
        bind(PackageScanner.class).to(PackageScannerImpl.class);
        bind(CommandRegistrar.class).to(CommandRegistrarImpl.class);
        bind(EventRegistrar.class).to(EventRegistrarImpl.class);

        bind(HomeLocationFactory.class).to(HomeLocationFactoryImpl.class);
        bind(MessageHelper.class).to(MessageHelperImpl.class);
        bind(AsyncHomeService.class).to(AsyncHomeServiceProxy.class);
        bind(BukkitScheduler.class).toInstance(javaPlugin.getServer().getScheduler());

        bind(HomeService.class)
                .annotatedWith(Names.named("HomeServiceTransactional"))
                .to(HomeServiceTransactionalProxy.class);

        bind(new TypeLiteral<GenericModelMapper<PlayerHomeLocation, HomeLocationEntity>>() {
        })
                .to(new TypeLiteral<PlayerHomeLocationModelMapper>() {
                });

        bind(GenericModelMapper.class)
                .annotatedWith(Names.named("PlayerHomeLocationModelMapper"))
                .to(PlayerHomeLocationModelMapper.class);

        bind(ChatManipulationStrategy.class)
                .toProvider(CompositeChatManipulationStrategyProvider.class)
                .asEagerSingleton();

    }
}