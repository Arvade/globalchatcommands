package org.bitbucket.arvade.globalchatcommands.event;

import com.google.inject.Inject;
import org.bitbucket.arvade.globalchatcommands.service.MessageService;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class SendMessageEventListener implements Listener {

    private MessageService messageService;

    @Inject
    public SendMessageEventListener(MessageService messageService) {
        this.messageService = messageService;
    }

    @EventHandler
    public void onPlayerSendMessageEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (!messageService.isChatEnabled()) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "Chat is disabled!");
        }

        if (messageService.isMuted(player)) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You're muted");
        }
    }
}
