package org.bitbucket.arvade.globalchatcommands.service;

import org.bukkit.entity.Player;

public interface MessageService {
    void toggleBlock(Player player);

    void sendMessage(Player sender, Player target, String message);

    void mutePlayer(Player player);

    void mutePlayer(Player player, String reason);

    void mutePlayer(Player player, long milis, String reason);

    boolean isMuted(Player player);

    boolean isChatEnabled();

    void setChat(boolean state);
}