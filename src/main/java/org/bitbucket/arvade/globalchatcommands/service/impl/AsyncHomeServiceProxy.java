package org.bitbucket.arvade.globalchatcommands.service.impl;

import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.service.AsyncHomeService;
import org.bitbucket.arvade.globalchatcommands.service.HomeService;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class AsyncHomeServiceProxy implements AsyncHomeService {

    private HomeService delegate;
    private BukkitScheduler bukkitScheduler;
    private JavaPlugin plugin;

    @Inject
    public AsyncHomeServiceProxy(@Named("HomeServiceTransactional") HomeService delegate,
                                 BukkitScheduler bukkitScheduler, JavaPlugin plugin) {
        this.delegate = delegate;
        this.bukkitScheduler = bukkitScheduler;
        this.plugin = plugin;
    }

    @Override
    public void asyncSave(HomeLocationEntity homeLocation) {
        bukkitScheduler.runTaskAsynchronously(plugin, () -> delegate.save(homeLocation));
    }

    @Override
    public void asyncDelete(HomeLocationEntity homeLocation) {
        bukkitScheduler.runTaskAsynchronously(plugin, () -> delegate.delete(homeLocation));
    }

    @Override
    public CompletableFuture<Optional<HomeLocationEntity>> asyncFindById(Integer id) {
        CompletableFuture<Optional<HomeLocationEntity>> future = new CompletableFuture<>();

        bukkitScheduler.runTaskAsynchronously(plugin, () -> {
            Optional<HomeLocationEntity> result = delegate.findById(id);
            bukkitScheduler.runTask(plugin, () -> future.complete(result));
        });

        return future;
    }

    @Override
    public CompletableFuture<Optional<HomeLocationEntity>> asyncFindByPlayerUUID(UUID playerUUID) {
        CompletableFuture<Optional<HomeLocationEntity>> future = new CompletableFuture<>();

        this.bukkitScheduler.runTaskAsynchronously(plugin, () -> {
            Optional<HomeLocationEntity> result = delegate.findPlayerByUUID(playerUUID);
            bukkitScheduler.runTask(plugin, () -> future.complete(result));
        });

        return future;
    }
}
