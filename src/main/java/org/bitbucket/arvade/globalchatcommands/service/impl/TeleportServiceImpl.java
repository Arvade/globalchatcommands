package org.bitbucket.arvade.globalchatcommands.service.impl;

import org.bitbucket.arvade.globalchatcommands.service.TeleportService;
import org.bukkit.entity.Player;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class TeleportServiceImpl implements TeleportService {

    private Map<Player, List<Player>> teleportRequests = new HashMap<>();

    public boolean addRequest(Player requestSender, Player target) {
        List<Player> listOfTargets = teleportRequests.get(requestSender);
        if (listOfTargets == null) {
            listOfTargets = new ArrayList<>();
            addTargetAndAndUpdate(listOfTargets, target, requestSender);
        } else {
            if (contains(listOfTargets, target)) {
                return false;
            }
            addTargetAndAndUpdate(listOfTargets, target, requestSender);
        }
        return true;
    }

    private void addTargetAndAndUpdate(List<Player> listOfTargets, Player target, Player requestSender) {
        listOfTargets.add(target);
        teleportRequests.put(requestSender, listOfTargets);
    }

    public boolean response(Player requestSender, Player target) {
        if (teleportRequests.containsKey(requestSender)) {
            List<Player> listOfTargets = teleportRequests.get(target);

            return teleportAndRemoveTargetIfContains(requestSender, target, listOfTargets);
        }

        return false;
    }

    private boolean teleportAndRemoveTargetIfContains(Player requestSender, Player target, List<Player> listOfTargets) {
        if (contains(listOfTargets, target)) {
            teleportAndRemoveTargetFromList(requestSender, target, listOfTargets);
            return true;
        }
        return false;
    }

    private void teleportAndRemoveTargetFromList(Player requestSender, Player target, List<Player> listOfTargets) {
        requestSender.teleport(target);
        listOfTargets.remove(target);
    }

    private boolean contains(List<Player> listOfTargets, Player target) {
        return listOfTargets.contains(target);
    }
}
