package org.bitbucket.arvade.globalchatcommands.service;

import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;

import java.util.Optional;
import java.util.UUID;

public interface HomeService {
    void save(HomeLocationEntity playerHomeLocation);

    void delete(HomeLocationEntity playerHomeLocation);

    Optional<HomeLocationEntity> findById(Integer id);

    Optional<HomeLocationEntity> findPlayerByUUID(UUID playerUUID);
}