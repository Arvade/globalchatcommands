package org.bitbucket.arvade.globalchatcommands.service.impl;

import org.bitbucket.arvade.globalchatcommands.common.MessageHelper;
import org.bitbucket.arvade.globalchatcommands.service.MessageService;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.Set;

import static org.bitbucket.arvade.globalchatcommands.command.argument.CommandMessagesPath.*;

@Singleton
public class MessageServiceImpl implements MessageService {


    private final Set<Player> playersWithBlockedPrivateMessages = new HashSet<>();
    private final Set<Player> mutedPlayers = new HashSet<>();

    private MessageHelper messageHelper;
    private FileConfiguration pluginConfig;

    private boolean isChatEnabled = true;

    @Inject
    public MessageServiceImpl(MessageHelper messageHelper, FileConfiguration pluginConfig) {
        this.messageHelper = messageHelper;
        this.pluginConfig = pluginConfig;
    }

    @Override
    public void toggleBlock(Player player) {
        if (hasBlockedChat(player)) {
            playersWithBlockedPrivateMessages.remove(player);
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(UNBLOCK_MSG));
        } else {
            playersWithBlockedPrivateMessages.add(player);
            messageHelper.translateAndSendMessage(player, pluginConfig.getString(BLOCK_MSG));
        }
    }

    @Override
    public void sendMessage(Player sender, Player target, String message) {
        if (hasBlockedChat(target)) {
            String formattedMessage = String.format(pluginConfig.getString(TARGET_HAS_BLOCKED_MSG), target.getDisplayName());
            messageHelper.translateAndSendMessage(sender, formattedMessage);
        } else {
            String formattedMessage = String.format(pluginConfig.getString(PRIVATE_MESSAGE_BOILERPLATE),
                    sender.getDisplayName(),
                    message);
            messageHelper.translateAndSendMessage(target, formattedMessage);
        }
    }

    @Override
    public void mutePlayer(Player player) {
        mutedPlayers.add(player);
    }

    @Override
    public void mutePlayer(Player player, String reason) {
        mutedPlayers.add(player);
        player.sendMessage(ChatColor.RED + "You has been muted. Reason: " + ChatColor.BLUE + reason);
    }

    @Override
    public void mutePlayer(Player player, long milis, String reason) {
        //TODO: impl
    }

    @Override
    public boolean isMuted(Player player) {
        return mutedPlayers.contains(player);
    }

    @Override
    public boolean isChatEnabled() {
        return isChatEnabled;
    }

    @Override
    public void setChat(boolean state) {
        isChatEnabled = state;
    }

    private boolean hasBlockedChat(Player player) {
        synchronized (playersWithBlockedPrivateMessages) {
            for (Object p : playersWithBlockedPrivateMessages) {
                if (p == player) {
                    return true;
                }
            }
            return false;
        }
    }
}
