package org.bitbucket.arvade.globalchatcommands.service.impl;

import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.service.HomeService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.inject.Inject;
import java.util.Optional;
import java.util.UUID;

public class HomeServiceTransactionalProxy implements HomeService {

    private SessionFactory sessionFactory;
    private HomeService delegate;

    @Inject
    public HomeServiceTransactionalProxy(SessionFactory sessionFactory, HomeService delegate) {
        this.sessionFactory = sessionFactory;
        this.delegate = delegate;
    }


    @Override
    public void save(HomeLocationEntity playerHomeLocation) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        this.delegate.save(playerHomeLocation);
        transaction.commit();
    }

    @Override
    public void delete(HomeLocationEntity playerHomeLocation) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        this.delegate.delete(playerHomeLocation);
        transaction.commit();
    }

    @Override
    public Optional<HomeLocationEntity> findById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<HomeLocationEntity> homeLocationEntityOptional = this.delegate.findById(id);
        transaction.commit();

        return homeLocationEntityOptional;
    }

    @Override
    public Optional<HomeLocationEntity> findPlayerByUUID(UUID playerUUID) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<HomeLocationEntity> homeLocationEntityOptional = this.delegate.findPlayerByUUID(playerUUID);
        transaction.commit();

        return homeLocationEntityOptional;
    }
}
