package org.bitbucket.arvade.globalchatcommands.service;

import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface AsyncHomeService {
    void asyncSave(HomeLocationEntity playerHomeLocation);

    void asyncDelete(HomeLocationEntity playerHomeLocation);

    CompletableFuture<Optional<HomeLocationEntity>> asyncFindById(Integer id);

    CompletableFuture<Optional<HomeLocationEntity>> asyncFindByPlayerUUID(UUID playerUUID);
}
