package org.bitbucket.arvade.globalchatcommands.service.impl;

import org.bitbucket.arvade.globalchatcommands.converter.GenericModelMapper;
import org.bitbucket.arvade.globalchatcommands.dto.HomeLocationEntity;
import org.bitbucket.arvade.globalchatcommands.model.PlayerHomeLocation;
import org.bitbucket.arvade.globalchatcommands.repository.HomeRepository;
import org.bitbucket.arvade.globalchatcommands.service.HomeService;
import org.bukkit.Server;

import javax.inject.Inject;
import java.util.Optional;
import java.util.UUID;

public class HomeServiceImpl implements HomeService {

    private HomeRepository homeRepository;
    private GenericModelMapper<PlayerHomeLocation, HomeLocationEntity> modelMapper;
    private Server server;

    @Inject
    public HomeServiceImpl(HomeRepository homeRepository,
                           GenericModelMapper<PlayerHomeLocation, HomeLocationEntity> modelMapper, Server server) {
        this.homeRepository = homeRepository;
        this.modelMapper = modelMapper;
        this.server = server;
    }

    @Override
    public void save(HomeLocationEntity transferObject) {
        PlayerHomeLocation playerHomeLocation = modelMapper.convertToEntityAttribute(transferObject);
        this.homeRepository.save(playerHomeLocation);
    }

    @Override
    public void delete(HomeLocationEntity transferObject) {
        PlayerHomeLocation playerHomeLocation = modelMapper.convertToEntityAttribute(transferObject);
        this.homeRepository.delete(playerHomeLocation);
    }

    @Override
    public Optional<HomeLocationEntity> findById(Integer id) {
        Optional<PlayerHomeLocation> playerHomeLocationOptional = this.homeRepository.findById(id);
        playerHomeLocationOptional
                .map(playerHomeLocation -> modelMapper.convertToDTO(playerHomeLocation))
                .ifPresent(Optional::of);

        return Optional.empty();
    }

    @Override
    public Optional<HomeLocationEntity> findPlayerByUUID(UUID playerUUID) {
        Optional<PlayerHomeLocation> playerHomeLocationOptional = this.homeRepository.findByPlayerUUID(playerUUID);

        return playerHomeLocationOptional
                .map(playerHomeLocation -> modelMapper.convertToDTO(playerHomeLocation));
    }
}
