package org.bitbucket.arvade.globalchatcommands.service;

import org.bukkit.entity.Player;

public interface TeleportService {

    boolean addRequest(Player requestSender, Player target);

    boolean response(Player requestSender, Player target);
}
