package org.bitbucket.arvade.globalchatcommands.repository.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.globalchatcommands.model.PlayerMute;
import org.bitbucket.arvade.globalchatcommands.repository.PlayerMuteRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;
import java.util.UUID;

public class PlayerMuteRepositoryImpl implements PlayerMuteRepository {

    private SessionFactory sessionFactory;

    @Inject
    public PlayerMuteRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(PlayerMute playerMute) {
        Session session = sessionFactory.getCurrentSession();
        session.save(playerMute);
    }

    @Override
    public void delete(PlayerMute playerMute) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(playerMute);
    }

    @Override
    public Optional<PlayerMute> findById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        PlayerMute playerMute = session
                .createQuery("SELECT * FROM PlayerMute WHERE id = :id", PlayerMute.class)
                .setParameter("id", id)
                .getSingleResult();

        return Optional.ofNullable(playerMute);
    }

    @Override
    public Optional<PlayerMute> findByPlayerUUID(UUID playerUUID) {
        Session session = sessionFactory.getCurrentSession();
        PlayerMute playerMute = session
                .createQuery("SELECT * FROM PlayerMute WHERE playerUUID = :playerUUID", PlayerMute.class)
                .setParameter("playerUUID", playerUUID)
                .getSingleResult();

        return Optional.ofNullable(playerMute);
    }
}