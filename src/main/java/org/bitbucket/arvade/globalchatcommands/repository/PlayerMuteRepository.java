package org.bitbucket.arvade.globalchatcommands.repository;

import org.bitbucket.arvade.globalchatcommands.model.PlayerMute;

import java.util.Optional;
import java.util.UUID;

public interface PlayerMuteRepository {

    void save(PlayerMute playerMute);

    void delete(PlayerMute playerMute);

    Optional<PlayerMute> findById(Integer id);

    Optional<PlayerMute> findByPlayerUUID(UUID playerUUID);

}
