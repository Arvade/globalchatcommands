package org.bitbucket.arvade.globalchatcommands.repository.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.globalchatcommands.model.PlayerHomeLocation;
import org.bitbucket.arvade.globalchatcommands.repository.HomeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;
import java.util.UUID;

public class HomeRepositoryImpl implements HomeRepository {

    private SessionFactory sessionFactory;

    @Inject
    public HomeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(PlayerHomeLocation playerHomeLocation) {
        Session session = sessionFactory.getCurrentSession();
        session.save(playerHomeLocation);
    }

    @Override
    public void delete(PlayerHomeLocation playerHomeLocation) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(playerHomeLocation);
    }

    @Override
    public Optional<PlayerHomeLocation> findById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        PlayerHomeLocation playerHomeLocation = session
                .createQuery("SELECT * FROM PlayerHomeLocation WHERE id = :id", PlayerHomeLocation.class)
                .setParameter("id", id)
                .getSingleResult();
        return Optional.ofNullable(playerHomeLocation);
    }

    @Override
    public Optional<PlayerHomeLocation> findByPlayerUUID(UUID playerUUID) {
        Session session = sessionFactory.getCurrentSession();
        PlayerHomeLocation playerHomeLocation = session
                .createQuery("SELECT * FROM PlayerHomeLocation WHERE playerUUID = :playerUUID", PlayerHomeLocation.class)
                .getSingleResult();

        return Optional.ofNullable(playerHomeLocation);
    }
}