package org.bitbucket.arvade.globalchatcommands.repository;

import org.bitbucket.arvade.globalchatcommands.model.PlayerHomeLocation;

import java.util.Optional;
import java.util.UUID;

public interface HomeRepository {
    void save(PlayerHomeLocation playerHomeLocation);

    void delete(PlayerHomeLocation playerHomeLocation);

    Optional<PlayerHomeLocation> findById(Integer id);

    Optional<PlayerHomeLocation> findByPlayerUUID(UUID playerUUID);
}
