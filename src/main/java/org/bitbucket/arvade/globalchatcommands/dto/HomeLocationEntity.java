package org.bitbucket.arvade.globalchatcommands.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class HomeLocationEntity {
    private UUID playerUUID;

    private Location location;
}
